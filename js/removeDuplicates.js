function removeDuplicates(data) {
	var dataNoDuplicates = [],
		exits = []
	data.forEach( function( item ) {
		if (!exits[item.id]) {
			dataNoDuplicates.push(item);
			exits[item.id] = true;
		}
	});
	return dataNoDuplicates;
}

var restData = [
	{
		id: 1,
		name: 'One'
	},
	{
		id: 1,
		name: 'One'
	},
	{
		id: 2,
		name: 'Two'
	}
];

var restDataNoDuplicates = removeDuplicates(restData);

console.log( "Initial: " + JSON.stringify(restData) );
console.log( "After:   " + JSON.stringify(restDataNoDuplicates) );